import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
  
const morabi1 = () => (
  <div class="jumbotron">
      <h1 class="display-4">mr akbari</h1>
      <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
      <hr class="my-4"/>
      <p>skill
        <ul>
          <li>aaa</li>
          <li>bbb</li>
          <li>ccc</li>
          <li>dd</li>
        </ul>
      </p>
  </div>
);
 
const morabi2 = () => (
  <div class="jumbotron">
      <h1 class="display-4">mr rostami</h1>
      <p class="lead">component for calling extra attention to featured content or information.</p>
      <hr class="my-4"/>
      <p>skill
        <ul>
          <li>aaadd</li>
          <li>bbbddd</li>
          <li>cccddd</li>
          <li>ddddd</li>
        </ul>
      </p>
  </div>
);
 
const morabi3 = () => (
 <div class="jumbotron">
      <h1 class="display-4">mr karimi</h1>
      <p class="lead">component for calling extra attention to featured content or information.this is testing for job</p>
      <hr class="my-4"/>
      <p>skill
        <ul>
          <li>adddddd</li>
          <li>bbbdddddfdfdfsd</li>
          <li>fsdfsdfdsfdsf</li>
          <li>fsdfdsfdsfds</li>
        </ul>
      </p>
  </div>
);

const morabi4 = () => (
   <div class="jumbotron">
      <h1 class="display-4">mr rahbar</h1>
      <p class="lead">component for calling extra attention to featured content or information.this is testing for job and yesc of course in the block</p>
      <hr class="my-4"/>
      <p>skill
        <ul>
          <li>ertyui</li>
          <li>asdfghjk</li>
          <li>xcvbn</li>
          <li>,mnbvc</li>
        </ul>
      </p>
  </div>
);
 
class App extends Component {
  render() {
    return (
      <div>
          <div class="kol">
          <ul>
              <li>
                  <div class="card" >
                      <img src="2.jpg" class="card-img-top" />
                      <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                          <Link to="/morabi1"  class="btn btn-primary">more</Link>
                      </div>
                  </div>
              </li>


              <li>
                  <div class="card" >
                      <img src="2.jpg" class="card-img-top" />
                      <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                          <Link to="/morabi2"  class="btn btn-primary">more</Link>
                      </div>
                  </div>
              </li>


              <li>
                  <div class="card" >
                      <img src="src/ima.jpg" class="card-img-top" />
                      <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                          <Link to="/morabi3"  class="btn btn-primary">more</Link>
                      </div>
                  </div>
              </li>


              <li>
                  <div class="card" >
                      <img src="2.jpg" class="card-img-top" />
                      <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                          <Link to="/morabi4"  class="btn btn-primary">more</Link>
                      </div>
                  </div>
              </li>


              

           
          


          
          </ul>

          </div> 
          <div class="contetn">
        <Route path="/morabi1" exact  component={morabi1}/>
        <Route path="/morabi2" component={morabi2}/>
        <Route path="/morabi3" component={morabi3}/>
        <Route path="/morabi4" component={morabi4}/>
        </div>
          


        
 
        
      </div>
      
    );
  }
}
 
export default App;