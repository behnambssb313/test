import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './style.css';
import App from './app.js';
 
ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById('root'),
);